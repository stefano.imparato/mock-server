const express = require('express');
const orderController = require("../controllers/contracts");

const router = express.Router();

router.get('/', orderController.findAll);
router.get('/search', orderController.search);
router.get('/:id', orderController.findOne);
router.post('/create', orderController.create);
router.put('/:id', orderController.update);
router.delete('/:id', orderController.delete);

module.exports = router;