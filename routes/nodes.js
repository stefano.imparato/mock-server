const express = require('express');
const orderController = require("../controllers/nodes");

const router = express.Router();

router.get('/', orderController.findAll);
router.get('/search/:term', orderController.search);
router.get('/:id', orderController.findOne);
router.post('/create', orderController.create);
router.put('/:id', orderController.update);
router.delete('/:id', orderController.delete);

module.exports = router;