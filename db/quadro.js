export const quadro = [
    {
        "AnaContrattiQuadroView": {
            "ID": 64,
            "NOME": "Contratto Quadro IRU 15 anni",
            "DURATA": 15,
            "ID_OPER_TELCO": 0,
            "OPERATORE": null,
            "DataStipula": "0001-01-01T00:00:00",
            "DATA_INIZIO": "2019-05-07T00:00:00+02:00",
            "DATA_FINE": "2019-05-09T00:00:00+02:00",
            "TIPOLOGIA_SERVIZIO": 1,
            "TIPOLOGIA_SERVIZIO_DESC": "Backhauling",
            "ATTIVO": false
        },
        "IdContrattoQuadro": null,
        "IdOperTelco": null,
        "AnaOperTelcoView": null
    },
    {
        "AnaContrattiQuadroView": {
            "ID": 18,
            "NOME": "Contratto Quadro IRU 15 anni",
            "DURATA": 15,
            "ID_OPER_TELCO": 0,
            "OPERATORE": null,
            "DataStipula": "0001-01-01T00:00:00",
            "DATA_INIZIO": "2019-05-10T00:00:00+02:00",
            "DATA_FINE": "2019-05-12T00:00:00+02:00",
            "TIPOLOGIA_SERVIZIO": 1,
            "TIPOLOGIA_SERVIZIO_DESC": "Backhauling",
            "ATTIVO": true
        },
        "IdContrattoQuadro": 18,
        "IdOperTelco": null,
        "AnaOperTelcoView": null
    },
    {
        "AnaContrattiQuadroView": {
            "ID": 18,
            "NOME": "Contratto Quadro IRU 3 anni",
            "DURATA": 3,
            "ID_OPER_TELCO": 0,
            "OPERATORE": null,
            "DataStipula": "0001-01-01T00:00:00",
            "DATA_INIZIO": "2019-05-08T00:00:00+02:00",
            "DATA_FINE": "2019-05-31T00:00:00+02:00",
            "TIPOLOGIA_SERVIZIO": 2,
            "TIPOLOGIA_SERVIZIO_DESC": "Rete Accesso",
            "ATTIVO": true
        },
        "IdContrattoQuadro": 18,
        "IdOperTelco": null,
        "AnaOperTelcoView": null
    }
]