export const contracts = [
    {
        "ID": 53,
        "ID_CONTRATTO_QUADRO": 18,
        "MODALITA": "EM",
        "DESC_MODALITA": "EURO/M",
        "ID_TIPOLOGIA": 1,
        "TIPOLOGIA": "fibra",
        "IMPORTO": 0.1,
        "ANTICIPATA": false,
        "ID_SOTTOTIPOLOGIA": 11,
        "SOTTOTIPOLOGIA": "1 coppia."
    },
    {
        "ID": 195,
        "ID_CONTRATTO_QUADRO": 18,
        "MODALITA": "EP",
        "DESC_MODALITA": "EURO/M/PROG",
        "ID_TIPOLOGIA": 1,
        "TIPOLOGIA": "fibra",
        "IMPORTO": 0,
        "ANTICIPATA": false,
        "ID_SOTTOTIPOLOGIA": null,
        "SOTTOTIPOLOGIA": null
    },
    {
        "ID": 196,
        "ID_CONTRATTO_QUADRO": 18,
        "MODALITA": "F",
        "DESC_MODALITA": "Forfait",
        "ID_TIPOLOGIA": 16,
        "TIPOLOGIA": "Colocazione in PCN in telaio",
        "IMPORTO": 450,
        "ANTICIPATA": false,
        "ID_SOTTOTIPOLOGIA": null,
        "SOTTOTIPOLOGIA": null
    },
    {
        "ID": 197,
        "ID_CONTRATTO_QUADRO": 18,
        "MODALITA": "F",
        "DESC_MODALITA": "Forfait",
        "ID_TIPOLOGIA": 6,
        "TIPOLOGIA": "Costi di attivazione",
        "IMPORTO": 860,
        "ANTICIPATA": false,
        "ID_SOTTOTIPOLOGIA": 24,
        "SOTTOTIPOLOGIA": "Inserimento nuovo giunto di spillamento  ."
    }
];