const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const contractSchema = new Schema(
    {
        title: { type: String, required: true, index: "text" },
        description: { type: String, required: false, index: "text" }
    },
    { timestamps: true }
);

module.exports = mongoose.model('contract', contractSchema);