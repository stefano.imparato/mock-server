const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser')
const orderRoutes = require('./routes/order');
const nodesRoutes = require('./routes/nodes');
const contractsRoutes = require('./routes/contracts');
const quadroRoutes = require('./routes/quadro');

const hostname = 'localhost';
const port = 3000;

const app = express();

app.use(bodyParser.json());

//CORS fix middleware
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use('/order', orderRoutes);
app.use('/nodes', nodesRoutes);
app.use('/contracts', contractsRoutes);
app.use('/quadro', quadroRoutes);

mongoose.connect('mongodb://admin:admin1@ds147926.mlab.com:47926/mock-db', { useCreateIndex: true, useNewUrlParser: true })
    .then(() => {
        app.listen(port, hostname, () => {
            console.log(`Server running at http://${hostname}:${port}/`);
        });
    })
    .catch(err => console.log(err));

