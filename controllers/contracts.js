const Contract = require('../models/contract');
const db = require('../db/contracts.js')

exports.findAll = (req, res, next) => {
    res.status(200).json(db);
}

exports.findOne = (req, res, next) => {
    const id = req.params.id;
    Contract.findById(id)
        .then(results => {
            res.status(200).json({
                message: "Contract fetched",
                data: results
            })
        })
        .catch(err => console.log(err));
}

exports.search = (req, res, next) => {
    const term = req.query.term;
    const query = {
        $text: { $search: term }
    };
    Contract.find(query)
        .then(results => {
            res.status(200).json({
                message: "Contract fetched",
                data: results
            })
        })
        .catch(err => console.log(err));
}

exports.create = (req, res, next) => {
    const payload = req.body;
    const contract = new Contract({
        title: payload.title,
        description: payload.description
    })

    contract.save()
        .then(results => {
            res.status(201).json({
                message: "Contract created successfully",
                data: results
            })
        })
        .catch(err => console.log(err));
}

exports.update = (req, res, next) => {
    const id = req.params.id;
    const payload = req.body;

    Contract.findById(id)
        .then(results => {
            results.title = payload.title;
            results.description = payload.description;
            results.updatedAt = Date.now();
            return results.save();
        })
        .then(results => {
            res.status(200).json({
                message: "Contract updated",
                data: results
            })
        })
        .catch(err => console.log(err));
}

exports.delete = (req, res, next) => {
    const id = req.params.id;

    Contract.findByIdAndRemove(id)
        .then(results => {
            res.status(200).json({
                message: "Contract deleted",
                data: results
            })
        })
        .catch(err => console.log(err));
}