const db = require('../db/quadro.js')

exports.findAll = (req, res, next) => {
    res.status(200).json(db.quadro);
}

exports.findOne = (req, res, next) => {
    const id = req.params.id;
}

exports.search = (req, res, next) => {
    const term = req.params.term;
    const filtered = db.quadro.filter(e => e.IdContrattoQuadro == term);
    res.status(200).json(filtered);
}

exports.create = (req, res, next) => {
    const payload = req.body;
}

exports.update = (req, res, next) => {
    const id = req.params.id;
    const payload = req.body;
}

exports.delete = (req, res, next) => {
    const id = req.params.id;
}