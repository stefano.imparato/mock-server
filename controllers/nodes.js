const db = require('../db/nodes.js')

exports.findAll = (req, res, next) => {
    res.status(200).json(db);
}

exports.findOne = (req, res, next) => {
    const id = req.params.id;
    Nodes.findById(id)
        .then(results => {
            res.status(200).json({
                message: "Nodes fetched",
                data: results
            })
        })
        .catch(err => console.log(err));
}

exports.search = (req, res, next) => {
    const term = req.params.term;
    const filtered = db.nodes.filter(e => e.COMUNE.toLowerCase().includes(term.toLowerCase()));
    res.status(200).json(filtered);
}

exports.create = (req, res, next) => {
    const payload = req.body;

}

exports.update = (req, res, next) => {
    const id = req.params.id;
    const payload = req.body;
}

exports.delete = (req, res, next) => {
    const id = req.params.id;
}