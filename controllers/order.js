const Order = require('../models/Order');
const db = require('../db/nodes.js')

exports.findAll = (req, res, next) => {
    Order.find()
        .then(results => {
            res.status(200).json({
                message: "Order list fetched",
                data: results
            })
        })
        .catch(err => console.log(err));
}

exports.findOne = (req, res, next) => {
    const id = req.params.id;
    Order.findById(id)
        .then(results => {
            res.status(200).json({
                message: "Order fetched",
                data: results
            })
        })
        .catch(err => console.log(err));
}

exports.search = (req, res, next) => {
    const term = req.query.term;
    const query = {
        $text: { $search: term }
    };
    Order.find(query)
        .then(results => {
            res.status(200).json({
                message: "Order fetched",
                data: results
            })
        })
        .catch(err => console.log(err));
}

exports.create = (req, res, next) => {
    const payload = req.body;
    const order = new Order({
        title: payload.title,
        description: payload.description
    })

    order.save()
        .then(results => {
            res.status(201).json({
                message: "Order created successfully",
                data: results
            })
        })
        .catch(err => console.log(err));
}

exports.update = (req, res, next) => {
    const id = req.params.id;
    const payload = req.body;

    Order.findById(id)
        .then(results => {
            results.title = payload.title;
            results.description = payload.description;
            results.updatedAt = Date.now();
            return results.save();
        })
        .then(results => {
            res.status(200).json({
                message: "Order updated",
                data: results
            })
        })
        .catch(err => console.log(err));
}

exports.delete = (req, res, next) => {
    const id = req.params.id;

    Order.findByIdAndRemove(id)
        .then(results => {
            res.status(200).json({
                message: "Order deleted",
                data: results
            })
        })
        .catch(err => console.log(err));
}